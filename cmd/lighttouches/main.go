package main

import (
	"fmt"
	"log"

	"gitlab.com/rmc/launchpad"
)

func main() {
	pad, err := launchpad.Connect()
	if err != nil {
		log.Fatalf("Error connecting to launchpad: %v", err)
	}

	s, err := pad.Subscribe()
	defer s.Stop()
	if err != nil {
		log.Fatalf("Error subscribing to messages: %v", err)
	}
	for {
		select {
		case m := <-s.Messages:
			fmt.Printf("%v\n", m)
			var key int
			color := launchpad.Color(launchpad.BrightnessFull, launchpad.BrightnessFull, launchpad.NoteOnFlagsNormal)
			var message launchpad.Message
			if m.Row < 8 {
				key = launchpad.GridKey(m.Row, m.Col)
				if m.On {
					message = launchpad.NoteOnMessage(key, color)
				} else {
					message = launchpad.NoteOffMessage(key)
				}
			} else {
				key = launchpad.TopKey(m.Col)
				if m.On {
					message = launchpad.TopRowOnMessage(key, color)
				} else {
					message = launchpad.TopRowOffMessage(key)
				}
			}
			if err = pad.SendMessage(message); err != nil {
				log.Fatalf("Error sending message: %v", err)
			}
		}
	}
}
