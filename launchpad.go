package launchpad

import (
	"gitlab.com/gomidi/midi/v2"
	"gitlab.com/gomidi/midi/v2/drivers"
	_ "gitlab.com/gomidi/midi/v2/drivers/rtmididrv" // autoregisters driver
)

type messageType = int

const (
	noteOff          messageType = 128
	noteOn           messageType = 144
	controllerChange messageType = 176
)
const topRowOffset = 104

type rawMessage = [3]byte

type Message interface {
	rawMessage() rawMessage
}

// Brightness represents the 4 levels of brightness that Launchpad's leds can be set to
type Brightness = int

const (
	// BrightnessOff Off
	BrightnessOff Brightness = iota
	// BrightnessLow Low
	BrightnessLow
	// BrightnessMedium Medium
	BrightnessMedium
	// BrightnessFull Full
	BrightnessFull
)

type BufferType = int

const (
	Buffer0 BufferType = iota
	Buffer1
)

type DoubleBufferingMessageFlags = int

const (
	DoubleBufferingDefault DoubleBufferingMessageFlags = 0
	DoubleBufferingFlash   DoubleBufferingMessageFlags = 8
	DoubleBufferingCopy    DoubleBufferingMessageFlags = 16
)

// NoteOnFlags represents the 3 different flags available when sending NoteOn messages
type NoteOnFlags = int

const (
	NoteOnFlagsDoubleBuffering NoteOnFlags = 0
	NoteOnFlagsFlash           NoteOnFlags = 8
	NoteOnFlagsNormal          NoteOnFlags = 12
)

// **ControllerChange messages**

type ControllerChangeMessage struct {
	Controller int
	Data       int
}

// rawMessage implements Message interface for ControllerChangeMessage
func (m *ControllerChangeMessage) rawMessage() rawMessage {
	messageType := controllerChange
	return [3]byte{byte(messageType), byte(m.Controller), byte(m.Data)}
}

// ResetMessage is used to reset the launchpad.
// All LEDs are turned off, and the grid mapping mode, buffer settings, and duty cycle are reset to their default values.
var ResetMessage = &ControllerChangeMessage{0, 0}

// SetXYLayoutMessage is used to set the grid mapping mode to X-Y layout (the default).
var SetXYLayoutMessage = &ControllerChangeMessage{0, 1}

// SetDrumRackLayoutMessage is used to set the grid mapping mode to Drum rack layout.
var SetDrumRackLayoutMessage = &ControllerChangeMessage{0, 2}

// DoubleBufferingMessage controls double-buffering
// Copy: copy the LED states from the new ‘displayed’ buffer to the new ‘updating’ buffer.
// Flash: continually flip ‘displayed’ buffers to make selected LEDs flash.
// Update: Set buffer 0 or buffer 1 as the new ‘updating’ buffer.
// Display Set buffer 0 or buffer 1 as the new ‘displaying’ buffer.
// The default state is zero: no flashing; the update buffer is 0; the displayed buffer is also 0.
// In this mode, any LED data written to Launchpad is displayed instantly.
// Sending this message also resets the flash timer, so it can be used to resynchronise the flash rates
// of all the Launchpads connected to a system.
func DoubleBufferingMessage(update, display BufferType, flags DoubleBufferingMessageFlags) *ControllerChangeMessage {
	data := 4*update + display + 32 + flags
	return &ControllerChangeMessage{0, data}
}

func TurnOnAllLedsMessage(b Brightness) *ControllerChangeMessage {
	return &ControllerChangeMessage{0, 125 + b}
}

func TopRowOffMessage(key int) *ControllerChangeMessage {
	return &ControllerChangeMessage{Controller: key, Data: 0}
}

func TopRowOnMessage(key int, velocity int) *ControllerChangeMessage {
	return &ControllerChangeMessage{Controller: key, Data: velocity}
}

// **Note messages**

// NoteMessage represents either a note-on or a note-off message
type NoteMessage struct {
	On       bool
	Key      int
	Velocity int
}

// rawMessage implements Message interface for NoteMessage
func (m *NoteMessage) rawMessage() rawMessage {
	messageType := noteOn
	if !m.On {
		messageType = noteOff
	}
	return [3]byte{byte(messageType), byte(m.Key), byte(m.Velocity)}
}

func NoteOffMessage(key int) *NoteMessage {
	return &NoteMessage{On: false, Key: key, Velocity: 0}
}

func NoteOnMessage(key int, velocity int) *NoteMessage {
	return &NoteMessage{On: true, Key: key, Velocity: velocity}
}

// GridKey takes a col(y) and row(x) representing to the column and row of one of the grid buttons
// and returns the decimal code for the respective key
// 0,0 represents the top-left button
func GridKey(row, col int) int {
	return 16*row + col
}

// RightKey takes the col(y) of the desired rightmost column buttons
// and returns the decimal code for the respective key
func RightKey(row int) int {
	return 16*row + 8
}

// TopKey takes the row(x) representing grid buttons coordinates
// and returns the decimal code for the respective key
func TopKey(col int) int {
	return topRowOffset + col
}

func Color(g, r Brightness, f NoteOnFlags) int {
	return 16*g + r + f
}

type Launchpad struct {
	In  drivers.In
	Out drivers.Out
}

func Connect() (*Launchpad, error) {
	in, err := midi.FindInPort("Launchpad Mini")
	if err != nil {
		return nil, err
	}
	out, err := midi.FindOutPort("Launchpad Mini")
	if err != nil {
		return nil, err
	}
	return &Launchpad{in, out}, nil
}

func (p *Launchpad) SendMessage(m Message) (err error) {
	if !p.Out.IsOpen() {
		if err = p.Out.Open(); err != nil {
			return err
		}
	}
	rawMessage := m.rawMessage()
	p.Out.Send([]byte{rawMessage[0], rawMessage[1], rawMessage[2]})
	return nil
}

type ButtonPressedMessage struct {
	On  bool
	Row int
	Col int
}

type Subscription struct {
	Messages chan *ButtonPressedMessage
	Stop     func()
}

func (p *Launchpad) Subscribe() (*Subscription, error) {
	buttonPressedChn := make(chan *ButtonPressedMessage)
	stop, err := midi.ListenTo(p.In, func(msg midi.Message, timestamps int32) {
		var channel, controller, value, key, velocity uint8
		switch {
		case msg.GetControlChange(&channel, &controller, &value):
			buttonPressedChn <- &ButtonPressedMessage{On: true, Row: 8, Col: int(value) - topRowOffset}
		case msg.GetNoteStart(&channel, &key, &velocity):
			buttonPressedChn <- &ButtonPressedMessage{On: true, Row: int(key) / 16, Col: int(key) % 16}
		case msg.GetNoteEnd(&channel, &key):
			buttonPressedChn <- &ButtonPressedMessage{On: false, Row: int(key) / 16, Col: int(key) % 16}
		}
	}, midi.UseSysEx())

	s := Subscription{buttonPressedChn, stop}

	return &s, err
}
