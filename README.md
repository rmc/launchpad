# launchpad

Exposes the Novation Launchpad MIDI communication protocol in a Go package.

It's built atop [RtMidi](https://github.com/thestk/rtmidi)
